/* 
 * File:   Etiquetadora.c
 * Author: Patricio Garces
 *
 * Created on 1 de julio de 2021, 21:36
 */

/* ATENCION: TODAS LAS VERSIONES DE PIC FUNCIONAN A 1/4 DE PASO*/

#include <12f675.h>
#device ADC=10
#use delay(clock=4000000)
#FUSES INTRC_IO,WDT,PUT,NOMCLR,BROWNOUT,PROTECT,NOCPD

/* Directiva para habilitar la inversion del pote, en caso de estar conectado al reves
 * Para la big size version lo decomento para usar el preset limitador,para la version normal lo comento*/
//#define BIG_SIZE

#define VEL_MIN 600                         //la regarga del timer deberia ser 64936
#define VEL_MAX 150                         //la recarga del timer deberia ser 65386

#define LED  PIN_A1
#define CANAL_ADC 2                         //an3 es el pin_a4
#define ADC_PORT sAN2

#define MOT_EN PIN_A5
#define MOTOR PIN_A4

unsigned int32 pote=0; 
unsigned int32 vel_mot=64936,buff_vel=64936;                        //por defecto le cargo la velocidad minima,es de 32bits porque cuando calculo las cuentas del timer no alcanza con 16bits
int1 flag_timer=0;


/* timer1 es de 16bits */
#int_TIMER1
void pulsos(void){
    output_toggle(MOTOR);                           //cambio el estado del pin
    set_timer1(buff_vel);                            //no existe en este pic una precarga automatica, tengo que recargar el registro en cada desborde
    
}


void main(void){
    unsigned int32 contador=0;
    
    
    /*
     *  Configuracion WDT
     */
    setup_wdt(WDT_18MS);
    
    
    /* timer1 es de 16bits 
     * necesito un periodo de entre 600us y 2000us, estos valores se van a definir dependiendo la vel maxima del motor que se use
     * para lograr los 600us, tengo que tener en cuenta que el tick a 4mhz: 4mhz/4=1MHZ de se�al de clock en el timer
     * ahora el tick al igual que el timer0 va a ser:  1us
     * para lograr 600us tengo que hacer: 600us/1us=600ticks, sin problemas de desborde, tengo que usar division de 1
     * para lograr 2000us tengo que hacer 2000us/1us=2000ticks, da super redondo, ahora en el timer 1 tengo que cargar
     * 65535-ticks para que haga las cuentas que quiero.
     */
    setup_timer_1(T1_INTERNAL|T1_DIV_BY_1);                 // timer 1 para generar el pulso cuadrado ,hay que calcular la division
    set_timer1(vel_mot);                                           //lo precargo para que inicie con el valor de desborde
    
    setup_adc_ports(ADC_PORT);                                  //elijo una sola vez el canal del ADC ya que siempre es el mismo
    SETUP_ADC(ADC_CLOCK_INTERNAL);
    
    enable_interrupts(INT_TIMER1);
    disable_interrupts(GLOBAL);  
   
    set_adc_channel(CANAL_ADC);
    
    output_high(MOT_EN);
    output_high(LED);
#ifndef BIG_SIZE
    while(READ_ADC()< 1020){                              //espero mientras el pote esta en una posicion distinta de 0
#else
    while(READ_ADC() > 13){
#endif
        restart_wdt();                          // me aseguro de reiniciar el wdt mientras espero que se mueva el pote al minimo
    }
    output_low(LED);
    enable_interrupts(GLOBAL); 
    output_low(MOT_EN);
    

    while(True){
        if((contador%60)==0){
           if(flag_timer){                      //si el timer del motor esta corriendo 
                output_toggle(LED);             //hago parpadear el led
            }  
            else{
                output_high(LED);               // el led esta toggleando,puede quedar prendido o apagado, con esto me aseguro que quede prendido
            }
        }
        
        if(contador%300){
            pote=READ_ADC();                   //solo recoge el valor leido,es de 10 bits
            
#ifndef BIG_SIZE
                vel_mot=64976+(((1023-pote)*(VEL_MIN-VEL_MAX))/1024); 
#else
                vel_mot=64976+((pote*(VEL_MIN-VEL_MAX))/1024); 
#endif
   
            buff_vel=vel_mot;
           

#ifndef BIG_SIZE
            if(pote>=1010){                                 //el cursor del pote esta soldado al reves, cuando vale 0 es la vel maxima, cuando vale 1024 es la minima
#else
            if(pote<13){
#endif
                flag_timer=0;
                disable_interrupts(INT_TIMER1);             //si la velocidad es cero apago la int del timer1 para dejar de generar los pulsos
                output_high(MOT_EN);                        //si el pote esta en cero, deshabilito el driver 
            }
            else{
                if(flag_timer==0){                          //si la int del timer estaba deshabilitada la habilito
                    flag_timer=1;
                    enable_interrupts(INT_TIMER1);
                    output_low(MOT_EN);                     //me aseguro que el motor este habilitado si el pote no esta en cero
                }
            }
        }
        restart_wdt();
        delay_ms(1);
        contador++;
    }
}

